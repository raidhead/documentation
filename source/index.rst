RaidHead
========

Qu'est-ce que RaidHead ?
------------------------

RaidHead est né de l'association de deux projets similaires mais complémentaires dédiés aux guildes désireuses d'organiser leurs événements de groupe. Le site ainsi que son forum ont comme vocation de rassembler les communautés des deux outils dits "raid planner" afin d'échanger et regrouper les remarques, demandes de fonctionnalités, retours d'expérience et autres astuces des joueurs.

Cette association a permis la création de plusieurs projets permettant de supporter et compléter les raid-planners MushRaider et MMOrga mais aussi tous les autres projets de ce type grâce à une base de donnée OpenData : L'API RaidHead.



RaidHead API
------------

RaidHead API est un service gratuit permettant de lire des données relatives aux jeux vidéo (principalement des MMORPG) telles que les donjons, classes, serveurs ou encore les races.

Le service sous sa forme d'API est inspiré de l'architecture REST (Representational State Transfer) et est accessible dans les formats JSON et XML.

.. toctree::
   :maxdepth: 1

   Présentation <API/presentation.rst>
   Requêtes <API/request.rst>
