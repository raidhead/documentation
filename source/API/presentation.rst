RaidHead API
============

Qu'est-ce que RaidHead API ?
----------------------------

RaidHead API est un service gratuit permettant de lire des données relatives aux jeux vidéo (principalement des MMORPG) telles que les donjons, classes, serveurs ou encore les races.

Le service sous sa forme d'API est inspiré de l'architecture REST (Representational State Transfer) et est accessible dans les formats JSON et XML.

Pourquoi un tel service ?
-------------------------

Ce service d'adresse avant tous aux développeurs, il permet de simplement et rapidement récupérer toutes les informations basiques composant les jeux listés dans notre API et ainsi développer de nouveaux services plus facilement.

Agrandir la base de donnée !
----------------------------

Parce que les jeux ainsi que leurs mises à jour vont et viennent, il nous est très difficile de garder la base 100% à jour. C'est pourquoi nous proposons à tout un chacun de nous aider en mettant à disposition un outil permettant de soumettre vos suggestions et mises à jour.

|location_link|.

.. |location_link| raw:: html

   <a href="https://api.raidhead.com/fra/involve" target="_blank">Soumettre un nouveau jeu ou une modification</a>


Faire des requêtes sur l'API RaidHead
-------------------------------------

.. toctree::
   :maxdepth: 1

   Pour commencer <request.rst>
   Liste des jeux <Request/games_list.rst>
   Détail d'un jeu <Request/game_detail.rst>