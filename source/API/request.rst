Généralité technique sur l'API RaidHead
=======================================

Pour plus de compatibilité, l'API supporte les requêtes au format https et http::

    HTTPS : https://api.raidhead.com/
    HTTP : http://api.raidhead.com/

L'API retourne toutes ses infos au format JSON, encodé en UTF-8.
Exemple::

    {
        "archeage": {
            "title":"ArcheAge",
            "short":"archeage",
            "level_max":50,
            "icon_32":"\/\/api.raidhead.com\/img\/games\/archeage\/archeage_32x32.png?1408365479",
            "lastupdate":1408815565
        }
    }


Images
------

Pour les icones, l'url est "sans protocol" et vous pouvez l'utiliser telle-quelle dans votre balise img::

    <img src="//api.raidhead.com/img/games/lotro/lotro_32x32.png?1408087851" alt="lotro" title="Lotro"/>

.. image:: https://api.raidhead.com/img/games/lotro/lotro_32x32.png
   :alt: lotro


Requêtes disponibles
--------------------

.. topic:: Liste des jeux

    GET `https://api.raidhead.com/fra/games/index.json`

    Documentation : :ref:`games_list_top`

.. topic:: Détail d'un jeu

    GET `https://api.raidhead.com/fra/games/get/<slug>.json`

    Documentation : :ref:`game_detail_top`
