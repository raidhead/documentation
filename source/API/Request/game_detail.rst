.. _game_detail_top:

Détail d'un jeu
===============

Pour obtenir toutes les informations d'un jeu en particulier, vous devez utiliser l'adresse suivante::

    Français : https://api.raidhead.com/fra/games/get/<slug>.json
    English : https://api.raidhead.com/eng/games/get/<slug>.json

Où "<slug>" est à remplacer par le slug de votre jeu. Ex : wildstar

Tous les Donjons, Serveurs, Races et Classes sont identifiés de manière unique par un ID numérique.
Cet ID peut être utilisé dans votre application en tant que "remote_id" afin de faire le lien pour d'éventuel ajout ou mise à jour.

|link_pastbin_detail|.

.. |link_pastbin_detail| raw:: html

   <a href="http://pastebin.com/et28Zg3T" target="_blank">Exemple pour le jeu Wildstar</a>

Détail des informations
-----------------------

* lastupdate : Date globale de dernière mise à jour (timestamp) des infos du jeu.
* game :
    * title : Titre du jeu
    * short : Nom court, unique. Aussi appelé "slug"
    * level_max : Niveau maximum
    * rank_title : Niveaux supplémentaires (ex : Rang Vétéran dans The Elder Scrolls Online)
    * rank_max : Niveau maximum des niveaux supplémentaires
    * icon_16 : Lien vers l'icone du jeu 16px*16px
    * icon_32 : Lien vers l'icone du jeu 32px*32px
    * icon_48 : Lien vers l'icone du jeu 48px*48px
    * icon_64 : Lien vers l'icone du jeu 64px*64px
    * has_server : Avec serveur distant (MMO)
    * has_dungeon : Avec instance
    * has_perso : Nécessite de créer un personnage
    * has_race : Race de personnage
    * has_classe : Classe de personnage
    * has_role : Rôle du personnage ou joueur
* servers :
    * id : ID unique du serveur
    * title : Nom du serveur
    * region : EU / NA
    * locale : Langue
    * activities : Tableau json pouvant contenir pve, pvp, rvr, rp (plusieurs choix possible)
* dungeons :
    * id : ID unique de l'instance
    * title : Nom de l'instance
    * max_players : Nombre de joueurs maximum
    * level_min : Niveau minimum
    * level_max : niveau maximum
* races :
    * id : ID unique de la race
    * title : Nom de la race
    * classes : Tableau des ID unique des classes accessible à cette race
* classes :
    * id : ID unique de la classe
    * title : Nom de la classe
    * color : Couleur hexadécimal (#abc123)
    * races : Tableau des ID unique des races accessible à cette classe
    * icon_16 : Lien vers l'icone de classe 16px*16px
    * icon_32 : Lien vers l'icone de classe 32px*32px
    * icon_48 : Lien vers l'icone de classe 48px*48px
    * icon_64 : Lien vers l'icone de classe 64px*64px
