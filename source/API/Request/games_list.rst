.. _games_list_top:

Obtenir la liste des jeux
=========================

La liste de tous les jeux est accessible à l'adresse suivante::

    Français : https://api.raidhead.com/fra/games/index.json
    English : https://api.raidhead.com/eng/games/index.json

En interrogeant cette liste, vous obtiendrez un tableau contenant tous les jeux supportés.
Chaque jeu est identifié de manière unique par son champ "short"::

    {
        "archeage": {
            "title":"ArcheAge",
            "short":"archeage",
            "lastupdate":1419887101,
            "icon_16":"\/\/api.raidhead.com\/img\/games\/archeage\/archeage_16x16.png?1408372851"
        },
        "eso":{
            "title":"Elder Scrolls Online",
            "short":"eso",
            "lastupdate":1419887100,
            "icon_16":"\/\/api.raidhead.com\/img\/games\/eso\/eso_16x16.png?1408093632"
        },
        "lotro": {
            "title":"Lotro",
            "short":"lotro",
            "lastupdate":1410004994,
            "icon_16":"\/\/api.raidhead.com\/img\/games\/lotro\/lotro_16x16.png?1408087851"
        }
    }




Détail des informations
-----------------------

* title : Titre du jeu
* short : Nom court, unique. Aussi appelé "slug"
* lastupdate : Date (timestamp) de dernière mise à jour des infos du jeu.
* icon_16 : Lien vers l'icone du jeu en 16px*16px
* icon_32 : Lien vers l'icone du jeu en 32px*32px
* icon_48 : Lien vers l'icone du jeu en 48px*48px
* icon_64 : Lien vers l'icone du jeu en 64px*64px

Pour les icones, l'url est "sans protocol" et vous pouvez l'utiliser telle-quelle dans votre balise img::

    <img src="//api.raidhead.com/img/games/lotro/lotro_32x32.png?1408087851" alt="lotro" title="Lotro"/>

.. image:: https://api.raidhead.com/img/games/lotro/lotro_32x32.png
   :alt: lotro


Mise à jour d'un jeu
--------------------

Pour savoir si un jeu a été mis à jour et afin de limiter le nombre de requête, il est recommandé de faire une requête unique sur cette page API et de comparer le champ "lastupdate" avec votre champ interne.

Nous vous prions aussi de ne pas faire plus d'une requête toutes les 5 minutes.